# MustangX S-4.1 (Blizzard)

A rebuild of my last tutorial(s). A few things have changed and I may recreate the frontend. Also a few changes have been brought to my attention which I will look into.

Using ***Takomah*** as a strating point. Building a Blogging System using the CRUD method.

==============================



**MustangX** is a Multilingual Content Management System. Built as a full scale blog, but planned to have some sort of expansion system someday, we will have a CMS as is needed. We start off with utilizing the <a target="_blank" rel="noopener noreferrer" href="https://symfony.com">Symfony Framework</a> and <a target="_blank" rel="noopener noreferrer" href="https://getcomposer.com">Composer</a>.

## Features (planned features)
These are things that I normally need on any system I use for building a website.

  *  Multilanguage
  *  Translation method for the content
  *  Cookie information
  *  Legal information stuff
  *  Blog or News pages
  *  Static Pages
  *  Captcha or reCaptcha
  *  Themes or Skins
  *  Modules, extensions, plugins
  *  Admin Area
  *  User stuff
  *  Method to install update, theme, modules in the backend


## Documentation
Will have development docs as I develop in the [WIKI](wiki).

Other docs as soon as is usable.

## License
<i aria-hidden="true" data-hidden="true" class="fa fa-balance-scale fa-fw"></i>
This project is licensed under the
[**MIT License**](LICENSE).

<a target="_blank" rel="noopener noreferrer" href="http://choosealicense.com/licenses/mit/">Learn more</a>

-----

## Get started from GIT

#### Get the code
```
git clone https://gitlab.com/MustangX/cms-core-dev/Frameworks/blizzard blizzard
```

#### Update the code
```
cd blizzard

composer update
```

#### Make sure you have a DB

Add your database credentials to the *.env* file

and then do this (on the commandline):
```
php bin/console doctrine:database:create
```
```
php bin/console doctrine:schema:update --force

php bin/console fos:user:create Admin --super-admin

php bin/console fos:user:create User
```

and the start it.
```
php bin/console server:start
```
You can turn it off with `php bin/console server:stop`

#### Get the assets updated
```
php bin/console assets:install --symlink --relative public
```

#### Update everything
```
composer update --with-dependencies
```
